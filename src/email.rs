use failure::{err_msg, Error};
use lettre::smtp::authentication::Credentials;
use lettre::smtp::SmtpClient;
use lettre::{ClientSecurity, ClientTlsParameters, Message, SmtpTransport, Transport};
use lettre_email::Email;
use log::{error, info, trace, warn};
use native_tls::TlsConnector;
use crate::config::{self, CONFIG};

pub fn send_email(receiver: &String, subject: &String, body: &String) -> std::result::Result<String, Error> {
    let mut response = "Ok";
    let utf8_email_receiver = str::replace(&receiver, "%40", "@");
    warn!("send_email");
    let mine_email = CONFIG.MINE_EMAIL.clone();
    let smtp_server = CONFIG.SMTP_SERVER.clone();
    let user_name = CONFIG.USER_NAME.clone();
    let password = CONFIG.PASSWORD.clone();
    let smtp_port = CONFIG.SMTP_PORT;
    let email = Email::builder()
        .to(receiver.to_owned())
        .from(mine_email.to_owned())
        .subject(subject)
        .body(body.to_owned())
        .build();
    let creds = Credentials::new(user_name.to_string(), password.to_string());

    let addr = String::from(format!("{}:{}", smtp_server, smtp_port));
    let connector = TlsConnector::new().unwrap();
    let tls_params = ClientTlsParameters::new(smtp_server.to_owned(), connector);
    let security = ClientSecurity::Required(tls_params);
    let mut mailer = match SmtpClient::new(addr, security) {
        Ok(v) => v.credentials(creds).transport(),
        Err(e) => {
            error!("Connecting to SMTP server was error: {}", e);
            return Err(err_msg("fail"));
        }
    };
    match mailer.send(email.unwrap().into()) {
        Ok(_) => info!("Email sent successfully!"),
        Err(e) => error!("Could not send email: {:?}", e),
    }
    Ok(response.to_owned())
}
