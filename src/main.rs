#![allow(warnings)]
use config::CONFIG;
use email::send_email;
use log::{error, info, trace, warn};
use failure::{err_msg, Error};
use std::{env, process};
use clap::Parser;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

pub mod email;
pub mod config;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[clap(short, value_parser)]
    email_txt: String,

    #[clap(short, value_parser)]
    content_txt: String,
}

#[async_std::main]
async fn main() -> std::result::Result<(), Error> {
    env::set_var("RUST_LOG", env::var_os("RUST_LOG").unwrap_or_else(|| "info".into()));
    env_logger::init();
    
    let args = Args::parse();
    let lines = read_lines(args.email_txt);
    let data = std::fs::read_to_string(args.content_txt);
    if let (Ok(lines), Ok(data)) = (lines, data) {
        for l in lines {
            if let Ok(l) = l {
                send_email(&l, &CONFIG.TITLE, &data);
            }
        }
    }
    Ok(())
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}