use lazy_static::lazy_static;
use serde_derive::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;
use std::time::Duration;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SMTPSetting {
    pub MINE_EMAIL: String,
    pub SMTP_SERVER: String,
    pub SMTP_PORT: u16,
    pub USER_NAME: String,
    pub PASSWORD: String,
    pub TITLE: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Setting {
    smtp: SMTPSetting,
}

impl Default for SMTPSetting {
    fn default() -> Self {
        let file_path = "config.toml";
        let mut file = match File::open(file_path) {
            Ok(f) => f,
            Err(e) => panic!("no such file {} exception:{}", file_path, e),
        };
        let mut str_val = String::new();
        match file.read_to_string(&mut str_val) {
            Ok(s) => s,
            Err(e) => panic!("Error Reading ApplicationConfig: {}", e),
        };
        let setting: Setting = toml::from_str(&str_val).unwrap();
        setting.smtp
    }
}

lazy_static! {
    pub static ref CONFIG: SMTPSetting = SMTPSetting::default();
}
